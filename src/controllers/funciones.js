const { users } = require('../data/users')
const { products } = require('../data/products')
const { payMethod } = require('../data/paymethod')
const { orders, items } = require('../data/order')

function newAccount(req, res) {
    const { user, name, lastname, email, phone, adress, password } = req.body;
    if (user && name && lastname && email && phone && adress && password) {
        const userId = users.length + 1;
        const admin = false;
        const newUser = { userId, ...req.body, admin };
        users.push(newUser);
        return res.send('User created')
    } else {
        res.status(400).send('We have some troubles')
    }
};

function loginAccount(req, res) {
    for (const user of users) {
        if (req.body.user == user.user && req.body.password == user.password) {
            return res.json(user.userId).status(200);
        }
    }
    res.send('Password or Username invalid').status(401);
};

function viewUsers (req,res) {
    res.json(users)
};

function addProduct(req, res) {
    const { nombre, precio } = req.body;
    if (nombre && precio) {
        const id = products.length + 1;
        const newProduct = { id, ...req.body };
        products.push(newProduct);
        return res.json('Product added!');
    } else {
        res.status(400).send('There is a problem!');
    }
};

function deleteProduct(req, res) {
    const idProduct = Number(req.params.id);
    for (const product of products) {
        if (idProduct === product.id) {
            products.splice(product.id - 1, 1);
            return res.json("Product " + product.nombre + " deleted");
        }
    }
    res.status(404).send('Product not found!');
};

function modifyProduct(req, res) {
    const idProduct = Number(req.params.id);
    const { nombre, precio } = req.body;
    if (nombre && precio) {
        for (const product of products) {
            if (product.id === idProduct) {
                product.nombre = nombre;
                product.precio = precio;
            }
        }
        res.json('Product modified');
    } else {
        res.send('Cant find the product').status(404);
    }
};

function showProducts(req, res) {
    return res.json(products)
};


function showPayMethods(req, res) {
    return res.json(payMethod)
};

function addPayMethod(req, res) {
    const { metodo } = req.body;
    if (metodo) {
        const id = payMethod.length + 1;
        const newMethod = { id, ...req.body };
        payMethod.push(newMethod);
        return res.json('Pay Method added!');
    } else {
        res.status(400).send('There is a problem!');
    }
};

function deletePayMethod(req, res) {
    const idPayMethod = Number(req.params.id);
    for (const method of payMethod) {
        if (idPayMethod === method.id) {
            payMethod.splice(method.id - 1, 1);
            return res.json("Pay method deleted");
        }
    }
    res.status(404).send('Pay method not found!');
};

function modifyPayMethod(req, res) {
    const idPayMethod = Number(req.params.id);
    const { metodo } = req.body;
    if (metodo) {
        for (const method of payMethod) {
            if (method.id === idPayMethod) {
                method.metodo = metodo;
            }
        }
        res.json('Pay method modified');
    } else {
        res.send('Cant modified the method').status(404);
    }
};

function addOrder(req, res) {
    const id = orders.length + 1;
    const status = "Pendiente";
    const open = true;
    const userId = Number(req.headers.iduser);
    const payment = 1
    const newOrder = { id, status, open, userId, items, payment};
    orders.push(newOrder);
    return res.json('Order created');
};

function addProductToOrder(req, res) {
    idUser = Number(req.headers.iduser);
    id = items.length + 1;
    for (const order of orders) {
        if (idUser === order.userId) {
            order.items.push({ id, ...req.body });
            return res.send('Product Added');
        }
    }
    res.send('Order does not exist by this user').status(404);
};



function viewOrdersAdmin(req, res) {
    return res.json(orders)
}

function modifyStatus(req, res) {
    const idOrder = Number(req.params.idOrder);
    const { status } = req.body;
    if (status) {
        for (const order of orders) {
            if (order.id === idOrder) {
                order.status = status;
            }
        }
        res.json('Status Modified');
    } else {
        res.send('Cant find the product').status(404);
    }
};

function deleteProductById(req, res) {
    const idOrder = Number(req.params.idproduct);
    const idUser = Number(req.headers.iduser);
    for (const order of orders) {
        if (order.userId === idUser) {
            for (const item of items) {
                if (item.id === idOrder) {
                    order.items.splice(idOrder - 1, 1);
                    return res.json('Product deleted');
                }
            }
        }
    }
    res.send('Cannot delete the product!');
};

function changePayMethod (req,res) {
    const idUser = Number(req.headers.iduser);
    const { payment } = req.body;
    if (payment) {
        for (const order of orders) {
            if (order.userId === idUser) {
                order.payment = payment;
            }
        }
        res.json('Payment selected');
    } else {
        res.send('Cant change payment').status(404);
    }
};

function userHistorial (req,res) {
    const idUser = Number(req.headers.iduser)
    for (const order of orders) {
        if(idUser === order.userId) {
            return res.json(order.items);
        }
    }
    res.status(404).send('Not found');
};




module.exports =
{
    viewUsers,
    newAccount,
    deleteProduct,
    addProduct,
    modifyProduct,
    loginAccount,
    showProducts,
    showPayMethods,
    addPayMethod,
    deletePayMethod,
    modifyPayMethod,
    addOrder,
    viewOrdersAdmin,
    addProductToOrder,
    modifyStatus,
    deleteProductById,
    changePayMethod,
    userHistorial
};
