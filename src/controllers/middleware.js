const { orders } = require('../data/order');
const {users} = require('../data/users');

function authAdmin (req,res,next) {
    idUser = Number(req.headers.iduser);
    for (const user of users) {
        if(user.userId === idUser && user.admin === true){
        return next();
        }
    }
    res.send('You are not allowed!').status(401);
};

function authUser(req, res, next) {
    idUser = Number(req.headers.iduser);
    for (const user of users) {
        if(user.userId === idUser) {
            return next();
        }
    }
   return res.json('You need sign up!');
};

function checkEmail (req,res,next) {
    for (const user of users) {
        if(req.body.email == user.email) {
            return res.send('That email has already been taken');
        }
    }
    next()
};

function openOrder (req,res,next) {
    for (const order of orders) {
        if(order.open == false) {
            res.send('The order is closed');
        }
    }
    next();
};



module.exports = {authUser, authAdmin, checkEmail,openOrder};