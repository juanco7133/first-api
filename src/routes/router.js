const { Router } = require('express');
const router = Router();
const {authAdmin,authUser,openOrder,checkEmail} = require('../controllers/middleware')
const {
    loginAccount,
    newAccount,
    showProducts,
    addProduct,
    deleteProduct,
    modifyProduct,
    showPayMethods,
    addPayMethod,
    deletePayMethod,
    modifyPayMethod,
    viewOrdersAdmin,
    userHistorial,
    addOrder,
    addProductToOrder,
    deleteProductById,
    changePayMethod,
    modifyStatus,
    viewUsers
} = require('../controllers/funciones')

//login
router.post('/login', loginAccount);
router.post('/register', checkEmail, newAccount);
router.get('/users', authAdmin,viewUsers)

// C.R.U.D products
router.get('/products', authUser, showProducts);
router.post('/products', authUser, authAdmin, addProduct);
router.delete('/products/:id', authUser, authAdmin, deleteProduct);
router.put('/products/:id', authUser, authAdmin, modifyProduct);

//C.R.U.D pay methods
router.get('/pay', authAdmin, showPayMethods);
router.post('/pay', authAdmin, addPayMethod);
router.delete('/pay/:id', authAdmin, deletePayMethod);
router.put('/pay/:id', authAdmin, modifyPayMethod);

//C.R.U.D Orders
router.get('/order', authAdmin, viewOrdersAdmin);
router.post('/order', authUser, addOrder);
router.post('/order/add', authUser, openOrder, addProductToOrder);
router.delete('/order/:idproduct', authUser, openOrder, deleteProductById);
router.put('/order/payment', authUser, openOrder, changePayMethod);
router.get('/order/historial', authUser, userHistorial)


router.put('/order/:idOrder', authAdmin, modifyStatus);


module.exports = router;