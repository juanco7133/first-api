const users = [
    {
        userId: 1,
        user: "pepeperez",
        name: "Pedro",
        lastname: "Perez",
        email: "pedroperez@gmail.com",
        phone: "12345",
        adress: "ruta 3",
        password: "pedro1234",
        admin : false
    },
    {
        userId: 2,
        user: "panchobro",
        name: "Pablo",
        lastname: "Marquez",
        email: "ppmarquez@gmail.com",
        phone: "12345",
        adress: "kuanip 123",
        password: "1234pp",
        admin : false
    },
    {
        userId: 3,
        user: "adminjuan",
        name: "Juan",
        lastname: "Gonzalez",
        email: "admin@gmail.com",
        password: "admin123",
        admin:true
    }
];



module.exports = {users};