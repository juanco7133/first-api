const payMethod = [
    {
        id: 1,
        metodo:"cash"
    },
    {
        id: 2,
        metodo: "debit card"
    },
    {
        id:3,
        metodo: "credit card"
    },
    {
        id:4,
        metodo: "mercado pago"
    }
];

module.exports = {payMethod};