const express = require('express');
const app = express();
const yaml = require('js-yaml');
const fs = require('fs');
const swaggerUi = require('swagger-ui-express');
app.use(express.json());


//rutas

app.use('/api', require('./routes/router'));






function loadSwagger(app) {
    try {
        const doc = yaml.load(fs.readFileSync('./src/doc_api.yml', 'utf8'));
        app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(doc));
    } catch (e) {
        console.log(e);
    }
};

function main() {
    loadSwagger(app);
};
main();

app.listen(8080, () => {
    console.log('Running ...');
});

