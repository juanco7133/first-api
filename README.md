# Delilah-Resto First API
API de un sistema de ordenes de un comercio con NodeJs y express

## Ejecucion local

Instalar dependencias 

``` 
$ npm i express
$ npm i js-yaml
$ npm i swagger-ui-express

```

## Iniciar el servidor

```
$ npm run start

```

## Documentacion con Swagger

En el archivo doc_api.yml que se corre cuando se inicie el servidor en http://localhost:8080/doc_api

